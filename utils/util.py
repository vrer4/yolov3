import torch


def to_cpu(tensor):
    return tensor.detach().cpu()


def load_classes(path):
    with open(path, 'r') as file:
        lines = file.read().split('\n')
        lines = [line.strip() for line in lines]
        lines = [line for line in lines if line]

    return lines


def weight_init(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


def build_targets(pred_boxes, pred_cls, target, anchors, ignore_thres):
    ByteTensor = torch.cuda.ByteTensor if pred_boxes.is_cuda else torch.ByteTensor
    FloatTensor = torch.cuda.FloatTensor if pred_boxes.is_cuda else torch.FloatTensor

    nB = pred_boxes.size(0)  # batch
    nA = pred_boxes.size(1)  # anchor_num
    nC = pred_cls.size(-1)  # class_num
    nG = pred_boxes.size(2)  # grid_size

    # iou_scores, class_mask, obj_mask, noobj_mask, 
    # tx, ty, tw, th, tcls, tconf
    obj_mask = ByteTensor(nB, nA, nG, nG).fill_(0)
    no_obj_mask = ByteTensor(nB, nA, nG, nG).fill_(1)
    class_mask = FloatTensor(nB, nA, nG, nG).fill_(0)
    iou_scores = FloatTensor(nB, nA, nG, nG).fill_(0)
    tx = FloatTensor(nB, nA, nG, nG).fill_(0)
    ty = FloatTensor(nB, nA, nG, nG).fill_(0)
    tw = FloatTensor(nB, nA, nG, nG).fill_(0)
    th = FloatTensor(nB, nA, nG, nG).fill_(0)
    tcls = FloatTensor(nB, nA, nG, nG, nC).fill_(0)

    target_boxes = target[:, 2:6] * nG
    gxy = target_boxes[:, :2]
    gwh = target_boxes[:, 2:]

    ious = torch.stack([bbox_wh_iou(anchor, gwh) for anchor in anchors])
    best_ious, best_n = ious.max(0)

    b, target_label = target[:, :2].long().t()
    gx, gy = gxy.t()
    gw, gh = gwh.t()
    gi, gj = gxy.long().t()

    obj_mask[b, best_n, gj, gi] = 1
    no_obj_mask[b, best_n, gj, gi] = 0

    for i, anchor_ious in enumerate(ious.t()):
        no_obj_mask[b[i], anchor_ious > ignore_thres, gj[i], gi[i]] = 0

    tx[b, best_n, gj, gi] = gx - gx.floor()
    ty[b, best_n, gj, gi] = gy - gy.floor()
    tw[b, best_n, gj, gi] = torch.log(gw / anchors[best_n][:, 0] + 1e-16)
    th[b, best_n, gj, gi] = torch.log(gh / anchors[best_n][:, 1] + 1e-16)

    tcls[b, best_n, gj, gi, target_label] = 1
    class_mask[b, best_n, gj, gi] = (
            pred_cls[b, best_n, gj, gi].argmax(-1) == target_label
    ).float()
    iou_scores[b, best_n, gj, gi] = bbox_iou(
        pred_boxes[b, best_n, gj, gi], target_boxes, x1y1x2y2=False
    )
    tconf = obj_mask.float()

    return iou_scores, class_mask, obj_mask, no_obj_mask, tx, ty, tw, th, tcls, tconf


def bbox_wh_iou(wh1, wh2):
    wh2 = wh2.t()
    w1, h1 = wh1[0], wh1[1]
    w2, h2 = wh2[0], wh2[1]
    inter_area = torch.min(w1, w2) * torch.min(h1, h2)
    union_area = w1 * h1 + w2 * h2 + 1e-16 - inter_area
    return inter_area / union_area


def bbox_iou(pred_boxes, target_boxes, x1y1x2y2=True):
    if not x1y1x2y2:
        b1_x1, b1_x2 = pred_boxes[:, 0] - pred_boxes[:, 2] / 2, pred_boxes[:, 0] + pred_boxes[:, 2] / 2
        b1_y1, b1_y2 = pred_boxes[:, 1] - pred_boxes[:, 3] / 2, pred_boxes[:, 1] + pred_boxes[:, 3] / 2
        b2_x1, b2_x2 = target_boxes[:, 0] - target_boxes[:, 2] / 2, target_boxes[:, 0] + target_boxes[:, 2] / 2
        b2_y1, b2_y2 = target_boxes[:, 1] - target_boxes[:, 3] / 2, target_boxes[:, 1] + target_boxes[:, 3] / 2
    
    else:
        b1_x1, b1_y1, b1_x2, b1_y2 = pred_boxes[:, 0], pred_boxes[:, 1], pred_boxes[:, 2], pred_boxes[:, 3]
        b2_x1, b2_y1, b2_x2, b2_y2 = target_boxes[:, 0], target_boxes[:, 1], target_boxes[:, 2], target_boxes[:, 3]

    x1 = torch.max(b1_x1, b2_x1)
    y1 = torch.max(b1_y1, b2_y1)
    x2 = torch.min(b1_x2, b2_x2)
    y2 = torch.min(b1_y2, b2_y2)

    inter_area = torch.clamp(x2-x1+1, min=0) * torch.clamp(y2-y1+1, min=0)

    box1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    box2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    return inter_area / (box1_area + box2_area - inter_area + 1e-16)


def xywh2xyxy(x):
    y = x.new(x.shape)
    y[..., 0] = x[..., 0] - x[..., 2] / 2
    y[..., 1] = x[..., 1] - x[..., 3] / 2
    y[..., 2] = x[..., 0] + x[..., 2] / 2
    y[..., 3] = x[..., 1] + x[..., 3] / 2
    return y 


def non_max_suppression(detection, conf_thres=0.5, nms_thres=0.4):
    detection[..., :4] = xywh2xyxy(detection[..., :4])
    output = [None for _ in range(len(detection))]
    for idx, prediction in enumerate(detection):
        prediction = prediction[prediction[:, 4] >= conf_thres]
        if not prediction.size(0):
            continue
        
        score = prediction[:, 4] * prediction[:, 5:].max(1)[0]
        prediction = prediction[(-score).argsort()]
        class_confs, class_preds = prediction[:, 5:].max(1, keepdim=True)
        predictions = torch.cat((prediction[:, :5], class_confs.float(), class_preds.float()), 1)

        boxes = []
        while predictions.size(0):
            iou = bbox_iou(predictions[0, :4].unsqueeze(0), predictions[:, :4]) > nms_thres
            same_class = predictions[0, -1] == predictions[:, -1]
            invalid = iou & same_class
            weights = predictions[invalid, 4]
            predictions[0, :4] = (weights.unsqueeze(1) * predictions[invalid, :4]).sum(0) / weights.sum()
            
            boxes += [predictions[0]]
            predictions = predictions[~invalid]

            if boxes:
                output[idx] = torch.stack(boxes)
        
    return output


def rescale_boxes(boxes, image_size, origin_shape):
    origin_h, origin_w = origin_shape
    pad_h = max(origin_w - origin_h, 0) * (image_size / max(origin_h, origin_w))
    pad_w = max(origin_h - origin_w, 0) * (image_size / max(origin_w, origin_w))

    # only remove padding in image_size scale.
    h = image_size - pad_h
    w = image_size - pad_w

    boxes[:, 0] = (boxes[:, 0] - pad_w // 2) / w * origin_w
    boxes[:, 2] = (boxes[:, 2] - pad_w // 2) / w * origin_w
    
    boxes[:, 1] = (boxes[:, 1] - pad_h // 2) / h * origin_h
    boxes[:, 3] = (boxes[:, 3] - pad_h // 2) / h * origin_h

    return boxes


if __name__ == "__main__":
    a = torch.Tensor(
        [[267.1953,  97.3170, 345.4926, 175.3122],
        [ 21.9109, 188.7968,  97.6058, 336.2822],
        [ 32.5888, 194.6943, 100.6042, 333.4722],
        [268.1117,  99.8067, 336.4014, 177.1195],
        [271.2415,  98.8788, 342.8307, 177.9292]]
    )
    print(bbox_iou(a[0].unsqueeze(0), a[3].unsqueeze(0)))
