import os
import random
import torch
import argparse
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from torch.utils.data import DataLoader

from model import Darknet
from utils.util import *
from utils.datasets import ImageFloder


def inference(args):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    dataset = ImageFloder(args.image_folder, image_size=args.image_size)
    dataloader = DataLoader(dataset, batch_size=args.batch_size, \
                shuffle=False, num_workers=args.num_worker)

    model = Darknet(args.config_path, args.image_size).to(device)
    model.load_darknet_weight(args.weights)
    model.eval()

    classes = load_classes(args.class_path)
    image_path = []  # only for image_paths
    image_detection = []

    for batch_i, (path, image) in enumerate(dataloader):
        image = torch.Tensor(image).to(device)

        with torch.no_grad():
            detection = model(image)
            detection = non_max_suppression(detection, args.conf_thres, args.nms_thres)
        
        image_path.extend(path)
        image_detection.extend(detection)
    
    return image_path, image_detection, classes


def plot(args, image_path, image_detection, classes):

    cmap = plt.get_cmap("tab20b")
    colors = [cmap(i) for i in np.linspace(0, 1, 20)]
    
    for i, (path, detections) in enumerate(zip(image_path, image_detection)):

        print("(%d) Image: '%s'" % (i, path))

        image = np.array(Image.open(path))
        plt.figure()
        fig, ax = plt.subplots(1)
        plt.imshow(image)

        detections = rescale_boxes(detections, args.image_size, image.shape[:2])
        labels = detections[:, -1].unique()
        bbox_color = random.sample(colors, len(labels))

        for x1, y1, x2, y2, conf, cls_conf, label_id in detections:
            print("\t+ Label: %s, Conf: %.5f" % (classes[int(label_id)], cls_conf.item()))
            w = x2 - x1
            h = y2 - y1

            color = bbox_color[int(np.where(labels == int(label_id))[0])]
            bbox = patches.Rectangle((x1, y1), w, h, linewidth=2, edgecolor=color, facecolor='None')
            ax.add_patch(bbox)
            plt.text(x1, y1, s=classes[int(label_id)], color='white', verticalalignment='top',
                    bbox={'color': color, 'pad': 0})
        
        plt.axis('off')
        filename = path.split("/")[-1].split(".")[0]
        plt.savefig(f"output/{filename}.png", bbox_inches="tight", pad_inches=0.0)
        plt.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--image_folder', type=str, default='test')
    parser.add_argument('--image_size', type=int, default=416)
    parser.add_argument('--batch_size', type=int, default=1)
    parser.add_argument('--num_workers', type=int, default=1)
    parser.add_argument('--config_path', type=str, default='config/yolov3.cfg')
    parser.add_argument('--weights', type=str, default='weight/yolov3.weights')
    parser.add_argument('--class_path', type=str, default='data/coco.names')
    parser.add_argument('--num_worker', type=int, default=4)
    parser.add_argument('--conf_thres', type=float, default=0.8)
    parser.add_argument('--nms_thres', type=float, default=0.4)

    args = parser.parse_args()

    os.makedirs('output', exist_ok=True)

    img_path, img_detection, classes = inference(args)
    plot(args, img_path, img_detection, classes)